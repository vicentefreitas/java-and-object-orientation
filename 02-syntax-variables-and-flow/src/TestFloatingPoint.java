
public class TestFloatingPoint {

	public static void main(String[] args) {

		double salary;
		salary = 1250.70;
		System.out.println("My salary is " + salary);

		double division;
		division = 3.14 / 2;
		System.out.println("Division = " + division);

		int anotherDivision;
		anotherDivision = 5 / 2;
		System.out.println("Another division " + anotherDivision);

		double newTry;
		newTry = 5.0 / 2;
		System.out.println("New try " + newTry);

	}

}
