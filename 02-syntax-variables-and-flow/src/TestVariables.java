
public class TestVariables {

	public static void main(String[] args) {

		int age;
		age = 31;
		System.out.println(age);

		age = 30 + 10;
		System.out.println(age);

		age = (7 * 5) + 2;
		System.out.println(age);

		System.out.println("Age is " + age);

	}

}
